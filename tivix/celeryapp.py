from celery import Celery
from celery.schedules import crontab


app = Celery('app')

app.autodiscover_tasks()
